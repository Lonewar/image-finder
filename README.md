# Prueba Técnica
Este pequeño proyecto ha sido desarrollador con motivo para una prueba técnica.

En este caso, se requeria la creación de una simple aplicación donde se mostrasen imágenes descargadas desde un servicio mockeado siguiendo ciertas pautas de diseño.

Sin embargo, se consultó una toma de decisión que era el uso de un servicio externos proporcionado por la plataforma [Pixabay](https://pixabay.com/api/docs/)

## Versión actual
La versión actual es la `versión 0.1.1`. En ella se implementan todas las funcionalidades principales requeridas con ciertas modificaciones y completamente responsive.

La modificación principal en el diseño es la unificación de los `Grid's`. Debido a un diseño relativamente distinto en `Desktop` y `Mobile`, se ha tomado la decisión de unificarlo en el diseño de `Mobile`. Incluye también una diferencia en ese diseño y es que debido a motivos de `UX`, el logotipo pasa a estar en la parte superior del `Header` mientras que el campo de buscador se encuentra en la parte inferior.

## Tecnologías Principales
- Debido a lo que se comentó en la entrevista, mi intención era desarollar la aplicación en `Next` y, aunque no se puede aplicar su funcionalidad al completo debido a que es una `Single Page App`, se han utilizado parte de sus módulos.
- Redux
- Thunk
- Axios

## Mejoras a implementar
- Por desgracia, el servicio unicamente permite el uso de peticiones `GET` por lo que no permite funcionalidades como el aplicar `Likes`, etc.
- En el diseño principal se indicaba que tenia que mostrar en las imagenes un pequeño espacio donde se mostrase un valor económico. En este caso, se implementaria a futuro unos de los campos proporcionados por la API (por ejemplo, las visitas que ha recibido la foto).
- Añadir animcaciones en el campo de búsqueda.
- Refactorización de los estilos de los componentes (entre ellos, los colores de los botones cuando se reliza el click)
- Implementar test's en los componentes

## Ejecución del proyecto
La aplicación utiliza variables de entorno para poder realizar las peticiones a la API.
Para poder arrancarlo, necesitamos crear un archiv `.env` en la capa mas alta del proyecto.
En el debemos crear los siguientes campos:
```
API_HOST=https://pixabay.com/api/
API_KEY={key}
NODE_ENV={development || production}
```
Se puede obtener una clave aqui: [Pixabay](https://pixabay.com/api/docs/) . Es bastante sencilla de obtener.
Una vez tengamos configurado el `.env`. Debemos ejecutar el comando:
```
npm run dev
```
De esta forma, el servicio arrancará en la ruta general:
```
localhost:3000
```

