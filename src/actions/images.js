// Services
import getImageList from '@local/services/images';
// Constants
import {
  IMAGES_GET_DATA,
  IMAGES_GET_DATA_SUCCESS,
  IMAGES_GET_DATA_FAILURE,
} from '../constants/images';


const action = (type, payload) => ({ type, payload });

// Get Images Data
export const getImagesData = query => (dispatch, getState) => {
  dispatch(action(IMAGES_GET_DATA, query));

  const images = getState().images[query];

  if (images) {
    dispatch(action(IMAGES_GET_DATA_SUCCESS, images));
    return images;
  }

  return getImageList(query)
    .then(payload => dispatch(
      action(IMAGES_GET_DATA_SUCCESS, { [query]: { ...payload.data.hits } })
    )
      .then(() => Promise.resolve({ [query]: payload.data.hits })))
    .catch(err => dispatch(action(IMAGES_GET_DATA_FAILURE, err)));
};
