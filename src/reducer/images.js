import { IMAGES_GET_DATA_SUCCESS } from '../constants/images';

const initialState = {};
export default (state = initialState, { type, payload }) => {
  switch (type) {
  case IMAGES_GET_DATA_SUCCESS:
    return {
      ...state, ...payload,
    };
  default:
    return state;
  }
};
