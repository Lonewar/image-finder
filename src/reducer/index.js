import { combineReducers } from 'redux';
import images from './images';

// Using combine reducers with only one reducer, just in case for a future case to use more reducers
export default combineReducers({
  images,
});
