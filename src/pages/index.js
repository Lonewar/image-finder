// Core
import React from 'react';
// Redux
import { createStore, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
// Thunk
import thunk from 'redux-thunk';
// Reducer
import reducer from '../reducer';
// App
import AppComponent from '../components/App';

// Store Initialization
// -------------------------------------
const composeEnhancer = process.env.NODE_ENV === 'development' ? composeWithDevTools : compose;
const store = createStore(reducer, composeEnhancer(applyMiddleware(thunk)));

const App = () => (
  <AppComponent
    store={store}
  />
);


export default App;
