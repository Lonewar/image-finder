import api from './api';

// Get images list
export const getImagesList = (query) => api({
  method: 'GET',
  params: {
    key: process.env.API_KEY,
    q: query,
  },
});

export default getImagesList;
