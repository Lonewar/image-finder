import axios from 'axios';
// Configuration
import mainConfig from './config';

const instance = axios.create(mainConfig);

export default (config) => instance({
  ...config,
});
