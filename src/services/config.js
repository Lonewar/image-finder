export default {
  baseURL: process.env.API_HOST,
  json: true,
};
