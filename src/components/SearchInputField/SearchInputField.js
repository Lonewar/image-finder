// Core
import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
// Hooks
import clsx from 'clsx';
// Icons
import { IoIosSearch as Search } from 'react-icons/io';
// Components
import InputField from '../InputField';
// Styles
import classes from './styles.scss';


const SearchInputField = ({
  customClasses,
  onChange,
}) => (
  <div
    className={clsx(classes.SearchInputField, customClasses.InputField)}
  >
    <Search className={clsx(classes.icon, customClasses.icon)} />
    <InputField
      className={classes.SearchInputField}
      type="text"
      placeholder="Are you looking for something?"
      onChange={onChange}
      customClasses={customClasses}
    />
  </div>
);


SearchInputField.propTypes = {
  customClasses: PropTypes.object,
};

SearchInputField.defaultProps = {
  customClasses: {},
};

export default SearchInputField;
