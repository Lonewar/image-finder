// Core
import React, { useState, useCallback } from 'react';
import { Provider, connect } from 'react-redux';
import PropTypes from 'prop-types';
// Components
import Header from '@local/components/Header';
import GridList from '@local/components/GridList';
// Actions
import { getImagesData } from '@local/actions/images';
// Styles
import styles from './App.scss';

const App = ({
  store,
  fetchImages,
  images,
}) => {
  const [text, setText] = useState('');

  const handlerOnChange = useCallback((_, value) => {
    setText(value);
    fetchImages(value);
  }, [text]);

  return (
    <Provider store={store}>
      <div className={styles.App}>
        <Header
          onChange={handlerOnChange}
        />
        <GridList images={images[text]} />
      </div>
    </Provider>
  );
};

const mapStateToProps = ({
  images,
}) => ({
  images,
});

const mapDispatchToProps = {
  fetchImages: query => getImagesData(query),
};


export default connect(mapStateToProps, mapDispatchToProps)(App);
