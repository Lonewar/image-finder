// Core
import React from 'react';
// Components
import SearchInputField from '@local/components/SearchInputField';
// Image
import Logo from '@local/resources/img/logo.jpg';
// Styles
import classes from './styles.scss';


const Header = ({
  onChange,
}) => (
  <div className={classes.Header}>
    <img className={classes.image} src={Logo} alt="Samyroad-logo" />
    <SearchInputField
      onChange={onChange}
      customClasses={classes}
    />
  </div>
);

export default Header;
