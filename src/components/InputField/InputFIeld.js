// Core
import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
// Hooks
import clsx from 'clsx';
import { useDebouncedCallback } from 'use-debounce';
// Style
import classes from './style.scss';

const InputField = ({
  placeholder,
  onChange,
  customClasses,
}) => {
  const [text, setText] = useState('');

  const [submit] = useDebouncedCallback((evt) => {
    onChange(evt, text);
  }, 600);

  const handlerOnChange = useCallback((evt) => {
    const { target: { value } } = evt;
    setText(value);
    submit(evt);
  }, [text, setText, onChange]);

  return (
    <div className={clsx(classes.FloatContainer, customClasses.FloatContainer)}>
      <input
        className={clsx(classes.InputField, customClasses.InputField)}
        type="text"
        name="textfield"
        onChange={handlerOnChange}
        placeholder={placeholder}
        value={text}
      />
    </div>
  );
};

InputField.propTypes = {
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  customClasses: PropTypes.object,
};

InputField.defaultProps = {
  placeholder: '',
  onChange: undefined,
  customClasses: {},
};


export default InputField;
