// Core
import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
// Hooks
import clsx from 'clsx';
// Styles
import classes from './styles.scss';


const GridButton = ({
  counter,
  onClick,
  customClasses,
  icon,
}) => {
  const [click, setClicked] = useState(false);

  const handlerClick = useCallback(() => {
    setClicked(!click);
    onClick();
  }, [click, onClick]);

  return (
    <button
      type="button"
      className={click
        ? clsx(classes.GridButton, customClasses.button)
        : classes.GridButton}
      onClick={handlerClick}
    >
      <icon.type
        className={click
          ? clsx(icon.props.className, customClasses.icon)
          : icon.props.className}
      />
      <div className={click
        ? clsx(classes.Counter, customClasses.counter)
        : classes.Counter}
      >
        {counter}
      </div>
    </button>
  );
};

GridButton.propTypes = {
  onClick: PropTypes.func,
  counter: PropTypes.number,
  customClasses: PropTypes.object,
  icon: PropTypes.object,
};

GridButton.defaultProps = {
  onClick: undefined,
  counter: 0,
  customClasses: {},
  icon: {},
};

export default GridButton;
