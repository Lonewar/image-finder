// Core
import React from 'react';
import PropTypes from 'prop-types';
// Icons
import { TiThumbsUp as IconLike } from 'react-icons/ti';
import { IoIosRefresh as IconShare } from 'react-icons/io';
// Components
import GridButton from '../GridButton';
// Styles
import classes from './styles.scss';


const Grid = ({
  image,
  title,
  author,
  likes,
  downloads,
  views,
}) => (
  <div className={classes.Grid}>
    <img loading="lazy" src={image} className={classes.GridImage} alt={title} />
    <div className={classes.InfoContainer}>
      <h3 className={classes.Title}>{title.toUpperCase()}</h3>
      <div className={classes.AuthorBox}>
        <p>by</p><h5 className={classes.Author}>{author}</h5>
      </div>
      <div className={classes.ActionsBox}>
        <GridButton
          onClick={() => { }}
          counter={likes}
          customClasses={{
            button: classes.LikeButtonClicked,
            icon: classes.LikeIconClicked,
            counter: classes.LikeCounterButtonClicked,
          }}
          icon={(
            <IconLike className={classes.IconLike} />
          )}
        />
        <GridButton
          onClick={() => { }}
          counter={downloads}
          customClasses={{
            button: classes.DownloadButtonClicked,
            icon: classes.DownloadIconClicked,
            counter: classes.DownloadCounterButtonClicked,
          }}
          icon={(
            <IconShare className={classes.IconShare} />
          )}
        />

      </div>
    </div>
  </div>
);


Grid.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  author: PropTypes.string,
  likes: PropTypes.number,
  downloads: PropTypes.number,
};

Grid.defaultProps = {
  image: '',
  author: '',
  title: '',
  likes: 0,
  downloads: 0,
};


export default Grid;
