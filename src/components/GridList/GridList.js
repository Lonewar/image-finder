// Core
import React from 'react';
import PropTypes from 'prop-types';
// Components
import Grid from '../Grid';
// Styles
import classes from './styles.scss';


const GridList = ({
  images,
}) => (
  <div className={classes.GridList}>
    {images && Object.values(images).map((image, id) => (
      <Grid
        key={id}
        image={image.largeImageURL}
        author={image.user}
        title={image.tags}
        likes={image.likes}
        downloads={image.downloads}
        views={image.views}
      />
    ))}
  </div>
);

GridList.propTypes = {
  images: PropTypes.object,
};

GridList.defaultProps = {
  images: {},
};

export default GridList;
