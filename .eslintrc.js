module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'airbnb',
  ],
  parser: "babel-eslint",
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'no-shadow': 0,
    'arrow-parens': 0,
    'comma-dangle': ["error", "always-multiline"],
    'no-underscore-dangle': 0,
    'react/no-array-index-key': 0,
    'import/no-extraneous-dependencies': 0,
    'import/no-named-as-default': 0,
    'no-unused-vars': 0,
    'import/prefer-default-export': 0,
    'react/jsx-filename-extension': [1, { "extensions": [".js", ".jsx"] }],
    'no-param-reassign': [2, { "props": false }],
    // Indent with 4 spaces
    "indent": ["error", 2],
    'jsx-a11y/anchor-is-valid': 0,
    'react/prop-types': 0,
    // Indent JSX with 4 spaces
    "react/jsx-indent": ["error", 2],

    // Indent props with 4 spaces
    "react/jsx-indent-props": ["error", 2],
    "react/jsx-props-no-spreading": [0, {
      "html": "ignore",
      "custom": "ignore",
    }],
    'react/jsx-one-expression-per-line': 0,
    "import/no-unresolved": 'off',
    'no-console': "error",
    'react/forbid-prop-types': [0, {
      "forbid": ['any', 'array', 'object'],
      "checkContextTypes": false,
      "checkChildContextTypes": false,
    }]
  },
};
